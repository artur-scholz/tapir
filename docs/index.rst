Tapir - An out of the box REST API backend
==========================================

Tapir is a `microservice <https://en.wikipedia.org/wiki/Microservices>`_ that
provides a web application backend with a simple and intuitive REST API.

It includes user management and a permission system and interfaces to a remote
datastore. The backend can be extended through plugins and domain specific
functionalities.


.. toctree::
   :maxdepth: 1
   :caption: Contents:

   introduction
   installation
   tutorial
