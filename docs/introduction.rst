.. _introduction:

Introduction
============

Tapir provides an out of the box web application framework for quickly building
a REST API backend. It allows developers to easily extend the application by
creating URL endpoints with dedicated functionality.

The main purpose of Tapir is to provide a user and data management backend
in front of the `Stretchy <https://gitlab.com/artur-scholz/stretchy.git>`_
data store. (It can also interface other data stores that comply with
the Stretchy API).

With Tapir it is possible to control the access to the data from the data store
in terms of read and write access. And it allows to leverage on this data by
letting the developers extend the backend with custom Python code to aggregate
and manipulate the data from the data store.
