.. _installation:

Installation
============

Bare Installation
-----------------

To install Tapir bare-bone on your machine, follow the instructions below. If
you instead prefer to quickly run it as a docker container, see the section
below.

You will need to have MariaDB (or MySQL) installed.

Clone or download the
`repository <https://gitlab.com/artur-scholz/tapir.git>`_ to your computer.

In the root directory run the following commands to prepare the
Python dependencies (you may want to use virtualenv for that).

.. code-block:: console

  $ pip install -r requirements.txt

Then copy the *.env.template* file to *.env* and make adjustments as needed.

And you're set! To start the server, run the following commands.

.. code-block:: console

  $ export FLASK_APP=tapir
  $ flask run -h 0.0.0.0

To verify that Tapir is running open your browser and visit http://localhost:5000.

Docker
------

As an alternative to the installation described above you can run Tapir as
a docker container. To do so, you first need to have installed docker and
docker-compose on your machine.

Then clone or download the
`repository <https://gitlab.com/artur-scholz/tapir.git>`_ to your computer.
In fact, you would only need the files *Dockerfile* and *docker-compose.yml*.
Then run ``docker-compose up``. Tapir will be available at *localhost:5000*.

First-Time Setup
================

You will need to initialize the database and create the admin account by hand
after a fresh install (applies to both, bare install and docker).

If you use a docker container, then enter it by running:

.. code-block:: console

  docker-compose run tapir bash

Initialize and populate the database:

.. code-block:: python

  $ flask db init
  $ flask db migrate
  $ flask db upgrade

Then create the admin user. This is done in the flask shell.

.. code-block:: python

  $ flask shell
  from tapir.models import User, Role
  from tapir.core import db
  user = User(email='admin@localhost', password='a-good-password-please')
  role = Role(domain='admin')
  user.roles = [role]
  db.session.add(user)
  db.session.add(role)
  db.session.commit()
  exit()
  $ exit

After that you can create and administer users via the web interface.
