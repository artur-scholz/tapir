# Tapir

Tapir is a `microservice <https://en.wikipedia.org/wiki/Microservices>`_ that
provides a web application backend with a simple and intuitive REST API.

## Features

Tapir includes a user management and permission system, which is stored in a
local database. It further interfaces to a remote data store for storing (large
amounts of) user data. The backend can be extended through plugins and domain
specific functionalities.

The main features are:

- User and Role management
- Permission management
- Authentication system (session and token based)
- Interface to remote data store
- Generic plugins for application-wide use
- Domain specific extensions for data parsing, synthesis, and processing

## Documentation

Find the full documentation here: [http://tapir.readthedocs.io][doc].

[doc]: http://tapir.readthedocs.io
