# This file is used to build the docker image of Tapir

FROM python:3.4-slim

RUN apt-get update &&\
  apt-get install -y git default-libmysqlclient-dev gcc &&\
  rm -rf /var/lib/apt/lists/*

RUN useradd dummy

WORKDIR /app
RUN git clone https://gitlab.com/artur-scholz/tapir.git /app
RUN pip3 install -r requirements.txt
RUN pip3 install gunicorn
RUN chown -R dummy:dummy /app

USER dummy
