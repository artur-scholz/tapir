from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from itsdangerous import BadSignature, SignatureExpired
from flask_login import UserMixin, AnonymousUserMixin
from flask import current_app

from tapir.core import db


class AnonymousUser(AnonymousUserMixin):
    pass


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(64), unique=True, index=True)
    firstname = db.Column(db.String(64), index=True)
    lastname = db.Column(db.String(64), index=True)
    password_hash = db.Column(db.String(128))
    active = db.Column(db.Boolean, default=True)

    def __repr__(self):
        return "User({} {})".format(self.firstname, self.lastname)

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_auth_token(self, expiration):
        s = Serializer(
            current_app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id}).decode('ascii')

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None  # valid token, but expired
        except BadSignature:
            return None  # invalid token
        user = User.query.get(data['id'])
        return user


user_roles = db.Table(
    'user_roles',
    db.Column(
        'user_id', db.Integer, db.ForeignKey('user.id'), primary_key=True),
    db.Column(
        'role_id', db.Integer, db.ForeignKey('role.id'), primary_key=True)
)


class Role(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    domain = db.Column(db.String(128))
    group = db.Column(db.String(128))

    users = db.relationship(
        'User', secondary=user_roles, lazy='subquery',
        backref=db.backref('roles', lazy=True))

    def __repr__(self):
        return "Role({}, {})".format(self.domain, self.group)
