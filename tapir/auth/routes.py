import base64
from functools import wraps

from flask import request, jsonify
from flask_login import logout_user, current_user, login_user

from tapir.core import login_manager
from tapir import http
from tapir.models import User, AnonymousUser
from . import auth


login_manager.anonymous_user = AnonymousUser


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


def authentication_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if current_user.is_anonymous:
            user = check_authorization()
            if user is None:
                user = check_token()
                if user is None:
                    return http.unauthorized('You are not logged in')
            if not user.active:
                return http.unauthorized('Your account is not active')
            login_user(user, remember=True)
        return func(*args, **kwargs)
    return decorated_view


def check_authorization():
    """Checks if basic authentication is provided in the request and returns
    the user if valid and active, otherwise returns None."""
    credentials = request.headers.get('Authorization')
    if not credentials:
        return None
    credentials = credentials.replace('Basic ', '', 1)
    credentials = base64.b64decode(credentials).decode('utf-8')
    email, password = credentials.split(':')
    user = User.query.filter_by(email=email).first()
    if not user or not user.verify_password(password) or not user.active:
        return None
    return user


def check_token():
    """Checks if provided token is valid and identifies a user that is valid
    and active and returns is, otherwise returns None."""
    token = request.headers.get('Token')
    if not token:
        return None
    user = User.verify_auth_token(token)
    if not user or not user.active:
        return None
    return user


@auth.route('/login', methods=['GET', 'POST'])
def login():
    """To log in, user needs to provide email and password."""
    user = check_authorization()
    if user is None:
        return http.unauthorized('Your login failed')
    else:
        login_user(user, remember=True)
        return http.ok('You are now logged in.')


@auth.route('/logout')
def logout():
    """Logout from the current session."""
    logout_user()
    return http.ok('You are now logged out.')


@auth.route('/test')
@authentication_required
def test():
    return http.ok('Your are authenticated as {}'.format(current_user))


@auth.route('/token')
@authentication_required
def get_token():
    return jsonify(
        {'token': current_user.generate_auth_token(
            expiration=3600), 'expiration': 3600})
