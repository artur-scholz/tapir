from flask import Blueprint

auth = Blueprint('auth', __name__, template_folder='templates')

from tapir.auth import routes
from tapir.auth.routes import authentication_required
