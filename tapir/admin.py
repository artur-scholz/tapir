from flask import redirect, url_for, render_template
from flask_login import current_user, logout_user, login_user
from flask_admin import Admin, AdminIndexView, expose
from flask_admin.menu import MenuLink
from flask_admin.contrib.sqla import ModelView
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import Required, Length

from tapir.models import User, Role
from tapir.permissions import is_admin


class MyModelView(ModelView):
    create_modal = True
    edit_modal = True

    # Overwrite the generic model view to require authentication
    def is_accessible(self):
        return is_admin(current_user)


class UserView(MyModelView):
    column_exclude_list = ['password_hash', ]
    column_searchable_list = ['email', 'firstname', 'lastname']
    form_columns = (
        'email', 'firstname', 'lastname', 'password_new', 'active', 'roles')
    form_excluded_columns = ('password_hash')
    form_extra_fields = {
            'password_new': PasswordField('Password')
        }

    def on_model_change(self, form, User, is_created):
        if form.password_new.data != '':
            User.password = form.password_new.data


class RoleView(MyModelView):
    can_view_details = True
    column_list = ('domain', 'group', 'users')


def create_admin_interface(app, db):
    admin = Admin(app, index_view=MyAdminIndexView())
    admin.add_view(UserView(User, db.session))
    admin.add_view(RoleView(Role, db.session))
    admin.add_link(
        MenuLink(name='Logout', url='/api/auth/logout', target='/'))


class MyAdminIndexView(AdminIndexView):
    @expose('/')
    def index(self):
        if not current_user.is_authenticated:
            return redirect(url_for('.login_view'))
        return super().index()

    @expose('/login/', methods=('GET', 'POST'))
    def login_view(self):
        form = LoginForm()
        if form.validate_on_submit():
            user = User.query.filter_by(
                email=form.email.data.lower()).first()
            if user is not None and user.verify_password(form.password.data):
                login_user(user)
                return redirect('/admin')
        return render_template('auth/login.html', form=form)

    @expose('/logout/')
    def logout_view(self):
        logout_user()
        return redirect('/')


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[Required(), Length(1, 64)])
    password = PasswordField('Password', validators=[Required()])
