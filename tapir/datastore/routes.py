import os
import csv

from flask import Response, request
from flask_login import current_user
import requests

from . import datastore
from tapir import http
from tapir.permissions import is_member_of_domain
from tapir.auth import authentication_required


DATASTORE_URL = os.environ.get('DATASTORE_URL')
RESTRICT_READ_OF_OTHER_DOMAINS = int(
    os.environ.get('RESTRICT_READ_OF_OTHER_DOMAINS', False))


def convert_url():
    return request.url.replace(
        request.url_root + "api/datastore", DATASTORE_URL)


@datastore.route(
    '/<domain>/<model>/<_id>/', strict_slashes=False, methods=['GET'])
@authentication_required
def get_model_entry(domain, model, _id):
    if RESTRICT_READ_OF_OTHER_DOMAINS:
        if not is_member_of_domain(current_user, domain):
            return http.forbidden(
                "You are not member of domain <{}>".format(domain))
    r = requests.get(convert_url())
    return Response(
        r.text, status=r.status_code, content_type=r.headers['content-type'])


@datastore.route('/<domain>/<model>/', strict_slashes=False, methods=['GET'])
@authentication_required
def get_model(domain, model):
    if RESTRICT_READ_OF_OTHER_DOMAINS:
        if not is_member_of_domain(current_user, domain):
            return http.forbidden(
                "You are not member of domain <{}>".format(domain))
    r = requests.get(convert_url())
    return Response(
        r.text, status=r.status_code, content_type=r.headers['content-type'])


@datastore.route('/<domain>/', strict_slashes=False, methods=['GET'])
@authentication_required
def get_domain(domain):
    if RESTRICT_READ_OF_OTHER_DOMAINS:
        if not is_member_of_domain(current_user, domain):
            return http.forbidden(
                "You are not member of domain <{}>".format(domain))
    r = requests.get(convert_url())
    return Response(
        r.text, status=r.status_code, content_type=r.headers['content-type'])


@datastore.route('/', strict_slashes=False, methods=['GET'])
@authentication_required
def get_root():
    r = requests.get(convert_url())
    return Response(
        r.text, status=r.status_code, content_type=r.headers['content-type'])


@datastore.route('/<domain>/<model>/', strict_slashes=False, methods=['POST'])
@authentication_required
def post_model_entry(domain, model):
    if not is_member_of_domain(current_user, domain):
        return http.forbidden(
            "You are not member of domain <{}>".format(domain))
    if len(request.files) > 0:  # handle csv files
        file_names = [k for k in request.files.keys()]
        for file_name in file_names:
            file = request.files[file_name]
            text = [line.decode('utf-8') for line in file]
            reader = csv.DictReader(text)
            documents = []
            for row in reader:
                documents.append(row)
        r = requests.post(convert_url(), json=documents)
    else:
        r = requests.post(convert_url(), json=request.json)
    return Response(
        r.text, status=r.status_code, content_type=r.headers['content-type'])


@datastore.route(
    '/<domain>/<model>/<_id>/', strict_slashes=False, methods=['PUT'])
@authentication_required
def put_model_entry(domain, model, _id):
    if not is_member_of_domain(current_user, domain):
        return http.forbidden(
            "You are not member of domain <{}>".format(domain))
    r = requests.put(convert_url(), json=request.json)
    return Response(
        r.text, status=r.status_code, content_type=r.headers['content-type'])


@datastore.route(
    '/<domain>/<model>/<_id>/', strict_slashes=False, methods=['PATCH'])
@authentication_required
def patch_model_entry(domain, model, _id):
    if not is_member_of_domain(current_user, domain):
        return http.forbidden(
            "You are not member of domain <{}>".format(domain))
    r = requests.patch(convert_url(), json=request.json)
    return Response(
        r.text, status=r.status_code, content_type=r.headers['content-type'])


@datastore.route(
    '/<domain>/<model>/<_id>/', strict_slashes=False, methods=['DELETE'])
@authentication_required
def delete_model_entry(domain, model, _id):
    if not is_member_of_domain(current_user, domain):
        return http.forbidden(
            "You are not member of domain <{}>".format(domain))
    r = requests.delete(convert_url())
    return Response(
        r.text, status=r.status_code, content_type=r.headers['content-type'])


@datastore.route(
    '/<domain>/<model>/', strict_slashes=False, methods=['DELETE'])
@authentication_required
def delete_model(domain, model):
    if not is_member_of_domain(current_user, domain):
        return http.forbidden(
            "You are not member of domain <{}>".format(domain))
    r = requests.delete(convert_url())
    return Response(
        r.text, status=r.status_code, content_type=r.headers['content-type'])
