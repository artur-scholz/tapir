import datetime

import requests


class DatastoreClient:

    def __init__(self, host, port):
        if not isinstance(port, int):
            raise TypeError("port must be an instance of int")
        self.host = host
        self.port = port
        self.url = "{}:{}".format(host, port)

    def __repr__(self):
        return "DatastoreClient({})".format(self.url)

    def __getattr__(self, name):
        if name.startswith('_'):
            raise AttributeError(
                "DatastoreClient has no attribute {}. To access the {} domain,"
                " use client[{}}].".format(name, name, name))
        return self.__getitem__(name)

    def __getitem__(self, name):
        return Domain(self, name)

    def drop(self, domain):
        r = requests.delete(self.url + '/' + domain)
        return r.status_code == 200


class Domain:
    def __init__(self, client, name):
        self.client = client
        self.name = name
        self.url = client.url + '/' + name

    def __repr__(self):
        return "Domain({}, {})".format(self.client, self.name)

    def __getattr__(self, name):
        if name.startswith('_'):
            raise AttributeError(
                "Domain has no attribute {}. To access the {} model, "
                "use domain[{}}].".format(name, name, name))
        return self.__getitem__(name)

    def __getitem__(self, name):
        return Model(self, name)

    def drop(self, model):
        r = requests.delete(self.url + '/' + model)
        return r.status_code == 200


class Model:
    def __init__(self, domain, name):
        self.domain = domain
        self.name = name
        self.url = domain.url + '/' + name

    def __repr__(self):
        return "Model({}, {})".format(self.domain, self.name)

    def __getitem__(self, _id):
        if not isinstance(_id, int):
            raise ValueError("index must be an instance of int")
        r = requests.get(self.url + '/' + str(_id))
        if r.status_code != 200:
            return None
        return r.json()[0]

    def insert(self, documents):
        """Inserts a single document or a list of documents. A document is
        a Python dictionary."""
        if not isinstance(documents, list):
            documents = [documents]
        r = requests.post(self.url, json=documents)
        return r.status_code == 201

    def insert_df(self, dataframe):
        """Inserts a Pandas dataframe."""
        return self.insert(dataframe.to_dict(orient='records'))

    def find(self, filter=None, fields=None, sort=None, limit=None, page=None,
             pagesize=None):
        # filtering
        _filter = ""
        if filter:
            if not isinstance(filter, dict):
                raise ValueError('filter must be an instance of dict')
            for key, value in filter.items():
                if isinstance(value, dict):
                    for k, v in value.items():
                        if not isinstance(v, list) and\
                                not isinstance(v, tuple):
                            value = "{}:{}".format(k, v)
                        else:
                            v = str(v)[1:-1]
                            v = v.replace(" ", "")
                            value = "{}:{}".format(k, v)
                        _filter += "{}={}&".format(key, value)
                elif isinstance(value, list) or isinstance(value, tuple):
                    value = ",".join([str(v) for v in value])
                    _filter += "{}={}&".format(key, value)
                else:
                    value = str(value)
                    _filter += "{}={}&".format(key, value)
            if _filter.endswith('&'):
                _filter = _filter[:-1]

        # projection
        _fields = ""
        if fields:
            if not isinstance(fields, list):
                raise ValueError('fields must be an instance of list')
            values = ','.join(fields)
            _fields = '_fields={}'.format(values)

        # sorting
        _sort = ""
        if sort:
            if not isinstance(sort, list):
                raise ValueError('sort must be an instance of list')
            values = ','.join(sort)
            _sort = '_sort={}'.format(values)

        # limiting and paging
        _limit = ""
        _page = ""
        _pagesize = ""
        if limit:
            _limit = "_limit={}".format(limit)
        elif page:
            _page = "_page={}".format(page)
            if pagesize:
                _pagesize = "_pagesize={}".format(pagesize)

        # build the query string
        query = ""
        if _filter:
            query += _filter + '&'
        if _fields:
            query += _fields + '&'
        if _sort:
            query += _sort + '&'
        if _page:
            query += _page + '&'
        if _pagesize:
            query += _pagesize + '&'
        if _limit:
            query += _limit + '&'

        if query:
            query = '?' + query
        if query.endswith('&'):
            query = query[:-1]

        r = requests.get(self.url + query)
        if r.status_code != 200:
            return None
        return r.json()

    def replace(self, _id, document):
        """Replaces the document that matches given ``_id`` with the
        new document."""
        if isinstance(document, list):
            document = document[0]
        for key, value in document.items():
            if isinstance(value, datetime.date):
                document[key] = value.isoformat()
        try:
            r = requests.put(self.url + '/' + str(_id), json=document)
        except Exception:
            return False
        return r.status_code == 200

    def modify(self, _id, document):
        """Modifies (updates) the document that matches given ``_id`` with
        the new document fields."""
        if isinstance(document, list):
            document = document[0]
        for key, value in document.items():
            if isinstance(value, datetime.date):
                document[key] = value.isoformat()
        try:
            r = requests.patch(self.url + '/' + str(_id), json=document)
        except Exception:
            return False
        return r.status_code == 200

    def delete(self, filter=None):
        """Deletes documents from the model.

        :param filter: Filter to apply. Set to ``None`` to match all.
        :rtype: bool
        """
        # filtering
        _filter = ""
        if filter:
            if not isinstance(filter, dict):
                raise ValueError('filter must be an instance of dict')
            for key, value in filter.items():
                if isinstance(value, dict):
                    for k, v in value.items():
                        if not isinstance(v, list) and not isinstance(v, list):
                            value = "{}:{}".format(k, v)
                        else:
                            v = str(v)[1:-1]
                            v = v.replace(" ", "")
                            value = "{}:{}".format(k, v)
                        _filter += "{}={}&".format(key, value)
                elif isinstance(value, list) or isinstance(value, tuple):
                    value = ",".join([str(v) for v in value])
                    _filter += "{}={}&".format(key, value)
                else:
                    value = str(value)
                    _filter += "{}={}&".format(key, value)
            if _filter.endswith('&'):
                _filter = _filter[:-1]

        # build the query string
        query = ""
        if _filter:
            query = '?' + _filter

        r = requests.delete(self.url + query)
        if r.status_code != 200:
            return False
        return True
