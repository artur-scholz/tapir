import os
from urllib.parse import urlparse

from .client import DatastoreClient


datastore_url = os.environ.get('DATASTORE_URL')
if datastore_url is None:
    raise Exception('Environment variable DATASTORE_URL is undefined')
datastore_url = urlparse(datastore_url)
host = datastore_url.scheme + '://' + datastore_url.hostname
port = datastore_url.port
client = DatastoreClient(host=host, port=port)


def delete_domain(domain):
    return client.drop(domain)


def delete_model(domain, model):
    domain = client[domain]
    return domain.drop(model)


def delete_documents(domain, model, filter=None):
    domain = client[domain]
    model = domain[model]
    return model.delete(filter)


def load_documents(
        domain, model, filter=None, fields=None, sort=None,
        limit=None, page=None, pagesize=None):
    domain = client[domain]
    model = domain[model]
    documents = model.find(filter, fields, sort, limit, page, pagesize)
    return documents


def save_documents(domain, model, documents, replace=False):
    domain = client[domain]
    model = domain[model]
    if replace:
        model.delete()
    return model.insert(documents)
