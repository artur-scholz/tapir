from flask import Blueprint

datastore = Blueprint('datastore', __name__)

from .core import *
from .routes import *
