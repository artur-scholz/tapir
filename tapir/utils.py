import os
import re
from datetime import datetime

from flask import request


def get_subfolders(folder_name):
    subfolders = [
        folder for folder in os.listdir(folder_name)
        if os.path.isdir(folder_name + '/' + folder)
        and not folder.startswith('__')]
    return subfolders


def datetime_from_string(datetime_string):
    # full timestamp with milliseconds and Z        yyyy-mm-ddThh:mm:ss.000Z
    match = re.match(
        r"\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d+Z", datetime_string)
    if match:
        return datetime.strptime(datetime_string, "%Y-%m-%dT%H:%M:%S.%fZ")

    # full timestamp with milliseconds without Z    yyyy-mm-ddThh:mm:ss.000Z
    match = re.match(
        r"\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d+", datetime_string)
    if match:
        return datetime.strptime(datetime_string, "%Y-%m-%dT%H:%M:%S.%f")

    # timestamp without milliseconds with Z         yyyy-mm-ddThh:mm:ssZ
    match = re.match(r"\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z", datetime_string)
    if match:
        return datetime.strptime(datetime_string, "%Y-%m-%dT%H:%M:%SZ")

    # timestamp without milliseconds without Z      yyyy-mm-ddThh:mm:ss
    match = re.match(r"\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}", datetime_string)
    if match:
        return datetime.strptime(datetime_string, "%Y-%m-%dT%H:%M:%S")

    # timestamp without hours and minutes           yyyy-mm-dd
    match = re.match(r"\d{4}-\d{2}-\d{2}", datetime_string)
    if match:
        return datetime.strptime(datetime_string, "%Y-%m-%d")

    # unknown timestamp format
    raise ValueError()


def isoformat(datetime):
    return datetime.strftime("%Y-%m-%dT%H:%M:%S.%f")


def get_arg(arg, datatype=None, default=None):
    arg_list = get_arg_list(arg, datatype, default)
    return arg_list[0] if arg_list else None


def get_arg_list(arg, datatype=None, default=None):
    arg = request.args.get(arg)
    if arg is None:
        if default is None:
            return None
        else:
            if isinstance(default, list):
                return default
            else:
                return [default]
    else:
        args = arg.split(',')
        if datatype is None:
            return args
        else:
            parsed_args = []
            for arg in args:
                if issubclass(datatype, datetime):
                    parsed_args.append(datetime_from_string(arg))
                else:
                    try:
                        parsed_args.append(datatype(arg))
                    except Exception:
                        return None
            return parsed_args
