from functools import wraps
from flask_login import current_user

from tapir.models import Role
from tapir.auth import authentication_required
from tapir import http


def is_admin(user):
    if current_user.is_anonymous:
        return False
    admin_domain = Role.query.filter_by(domain='admin').first()
    if admin_domain and current_user in admin_domain.users:
        return True
    return False


def is_member_of_domain(user, domain):
    domain_entries = Role.query.filter_by(domain=domain).all()
    for domain_entry in domain_entries:
        if user in domain_entry.users:
            return True
    else:
        return False


def is_member_of_domain_group(user, domain, group):
    domain_group_entries = Role.query.filter_by(
        domain=domain, group=group).all()
    for domain_group_entry in domain_group_entries:
        if user in domain_group_entry.users:
            return True
    else:
        return False


###############################################################################
# Decorators
###############################################################################

def domain_required(domain):
    def decorated(func):
        @authentication_required
        @wraps(func)
        def wrapper(*args, **kwargs):
            if not current_user.active:
                return http.unauthorized("Your account is not active")
            if not is_member_of_domain(current_user, domain):
                return http.forbidden("You are not member of the domain")
            return func(*args, **kwargs)
        return wrapper
    return decorated


def domain_group_required(domain, group):
    def decorated(func):
        @authentication_required
        @wraps(func)
        def wrapper(*args, **kwargs):
            if not current_user.active:
                return http.unauthorized("Your account is not active")
            if not is_member_of_domain_group(current_user, domain, group):
                return http.forbidden("You are not member of the domain group")
            return func(*args, **kwargs)
        return wrapper
    return decorated


def admin_required(func):
    @authentication_required
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if not current_user.active:
            return http.unauthorized("Your account is not active")
        if not is_admin(current_user):
            return http.forbidden("You are not an admin")
        return func(*args, **kwargs)
    return decorated_view
