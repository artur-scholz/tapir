from flask import jsonify


def ok(message=None):
    response = jsonify({'info': 'ok', 'message': message})
    response.status_code = 200
    return response


def created(message=None, location=None, link=None):
    response = jsonify({'info': 'created', 'message': message})
    response.status_code = 201
    if location:
        response.headers['location'] = location
    if link:
        response.headers['link'] = link
    return response


def not_modified(message=None):
    response = jsonify({'error': 'not modified', 'message': message})
    response.status_code = 304
    return response


def bad_request(message=None):
    response = jsonify({'error': 'bad request', 'message': message})
    response.status_code = 400
    return response


def unauthorized(message=None):
    response = jsonify({'error': 'unauthorized', 'message': message})
    response.status_code = 401
    return response


def forbidden(message=None):
    response = jsonify({'error': 'forbidden', 'message': message})
    response.status_code = 403
    return response


def not_found(message=None):
    response = jsonify({'error': 'not found', 'message': message})
    response.status_code = 404
    return response


def method_not_allowed(message=None):
    response = jsonify({'error': 'method not allowed', 'message': message})
    response.status_code = 405
    return response


def internal_server_error(message=None):
    response = jsonify({'error': 'internal server error', 'message': message})
    response.status_code = 500
    return response


def register_error_handlers(app):

    @app.errorhandler(400)
    def _bad_request(e):
        return bad_request(str(e))

    @app.errorhandler(401)
    def _unauthorized(e):
        return unauthorized(str(e))

    @app.errorhandler(403)
    def _forbidden(e):
        return forbidden(str(e))

    @app.errorhandler(404)
    def _page_not_found(e):
        return not_found(str(e))

    @app.errorhandler(405)
    def _method_not_allowed(e):
        return method_not_allowed(str(e))

    @app.errorhandler(500)
    def _internal_server_error(e):
        return internal_server_error(str(e))
