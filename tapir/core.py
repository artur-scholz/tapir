import os
import importlib

from flask import Flask, jsonify
from flask_sslify import SSLify
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager


db = SQLAlchemy()
migrate = Migrate()
login_manager = LoginManager()
login_manager.session_protection = 'strong'


def create_app(config_name):
    from config import config

    app = Flask(__name__)
    app.config.from_object(config[config_name])

    if int(os.environ.get('HTTPS_REDIRECT', 0)):
        SSLify(app)  # redirect http to https

    db.init_app(app)
    migrate.init_app(app, db)
    login_manager.init_app(app)

    from .http import register_error_handlers
    register_error_handlers(app)

    from .admin import create_admin_interface
    create_admin_interface(app, db)

    @app.route("/")
    def index():
        documents = []
        for rule in app.url_map.iter_rules():
            line = "{} {}".format(rule.rule, rule.methods)
            documents.append(line)
        return jsonify(sorted(documents))

    from .auth import auth
    app.register_blueprint(auth, url_prefix='/api/auth')

    from .datastore import datastore
    app.register_blueprint(datastore, url_prefix='/api/datastore')

    from .utils import get_subfolders
    for domain in get_subfolders('domains'):
        package = importlib.import_module('domains.' + domain)
        blueprint = getattr(package, domain)
        app.register_blueprint(blueprint, url_prefix='/api/domain/' + domain)

    return app
